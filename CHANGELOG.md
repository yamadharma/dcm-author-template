# [1.0.0](https://github.com/yamadharma/dcm-author-template/compare/v0.1.10...v1.0.0) (2021-08-04)


### Bug Fixes

* **class:** update document class ([c96cb23](https://github.com/yamadharma/dcm-author-template/commit/c96cb2348cf58fd0e9131aacebaf5d5d5916b21a))




